# VISHMO: Visualization of horizontal motion fields with R

* Dependencies: R 3.x, (raster, rasterVis)
* André Stumpf (andre.stumpf@unistra.fr)

# Examples


```
#!r

setwd('~/Documents/vishmo')
source('plotField.R')

EW <- ('data/BalochistanEW.tif')
NS <- ('data/BalochistanNS.tif')
```

## Displacement field with norm as background

```
#!r
plotField(EW, NS, type="magnitude",max.displacement = 12,title='Displacement vectors and norm', col.bias=0.8)
```
![VectorNorm.png](./Figures/VectorNorm.png)



## Displacement field with DEM in the background

```
#!r
background <- ('data/BalochistanSRTM.tif')
plotField(EW, NS, background, type='DEM',max.displacement = 12,title='Displacement vectors and norm', col.bias=0.8)
```
![VectorDSM.png](./Figures/VectorDSM.png) 


## Displacement field with transparent norm and hillshade in the background

```
#!r
background <- ('data/BalochistanSRTM.tif')
plotField(EW, NS, background, type='hillshade',max.displacement = 12,title='Displacement vectors and norm', col.bias=0.8)
```
![VectorNormHillshade.png](./Figures/VectorNormHillshade.png) 


## Displacement field with transparent norm and image in the background

```
#!r
background <- ('data/BalochistanImage.tif')
plotField(EW, NS, background, type='image',max.displacement = 12,title='Displacement vectors and norm', col.bias=0.8)
```

![VectorNormImage.png](./Figures/VectorNormImage.png)

## Draw fault line and get cross-section


```
#!r
# use subset
subset.extent <-new("Extent"
                    , xmin = 676386.526826068
                    , xmax = 696673.594911795
                    , ymin = 2913603.72321038
                    , ymax = 2931354.90778539
)

fault.line <- plotField(EW, NS, type="draw", max.displacement = 12, col.bias=0.8,
              title='Draw fault line', extent=subset.extent)
```

After interactive drawing this should look like this:

![DrawFaultLine.png](./Figures/DrawFaultLine.png)

The following can be used to obtain a plot of the cross-sections.

```
#!r
# save(fault.line, file = "data/faultLine.RData")
# load("data/faultLine.RData")

source('getCrossSection.R')
getCrossSection(EW, NS, fault.line, cs.length = 1000, cs.spacing = 1000, extent=subset.extent)
```
![CrossSection.png](./Figures/CrossSection.png)